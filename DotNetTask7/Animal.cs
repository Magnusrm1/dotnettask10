﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    abstract class Animal
    {
        public string Name { get; set; }

        public Animal(string name)
        {
            Name = name;
        }

    }
}
