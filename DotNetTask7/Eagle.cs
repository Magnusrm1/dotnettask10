﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    class Eagle : Bird, IFlyer
    {
        public Eagle(string name, string habitat, int wingspan) : base(name, habitat, wingspan)
        {

        }

        public override void LayEggs()
        {
            Console.WriteLine($"{Name} the eagle laid an egg!");
        }

        public override string GetBirdType()
        {
            return "Eagle";
        }
    }
}
