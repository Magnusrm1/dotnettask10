﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    //Inherits all properties of Animal
    abstract class Bird : Animal
    {
        public string Habitat { get; set; }
        public int Wingspan { get; set; }

        public Bird(string name) : base(name)
        {
            Habitat = "";
            Wingspan = -1;
        }

        public Bird(string name, string habitat, int wingspan) : base(name)
        {
            Habitat = habitat;
            Wingspan = wingspan;
        }

        // Make children override this
        public abstract void LayEggs();

        public abstract string GetBirdType();

        // Children may override this
        public virtual void Airstrike()
        {
            Console.WriteLine($"{Name} prepares for an airstrike and carefully takes aim...");
        }

    }
}
