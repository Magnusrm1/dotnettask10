﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    class Ostrich : Bird, IWalker
    {
        //Inherits all properties of Bird and Animal
        public Ostrich(string name) : base(name) { }
        public Ostrich(string name, string habitat, int wingspan) : base(name, habitat, wingspan) { }

        public override void LayEggs()
        {
            Random r = new Random();
            int amount = r.Next(15, 18);
            Console.WriteLine($"The ostrich {Name} laid {amount} eggs!");
        }

        public override string GetBirdType()
        {
            return "Ostrich";
        }

        // Overriding method to alter behavior of the ostrich airstrike.
        public override void Airstrike()
        {
            Console.WriteLine($"{Name} the ostrich leaps into the air as they cannot fly.");
            base.Airstrike();
        }

        public void Walk()
        {
            Console.WriteLine($"The ostrich {Name} starts walking around like it does.");
        }
    }
}
