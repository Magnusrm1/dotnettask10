﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DotNetTask7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Horse> stable = new List<Horse>();
            Horse horse1 = new Horse("Hank");
            Horse horse2 = new Horse("Henry");
            Horse horse3 = new Horse("Helen", 6);
            stable.Add(horse1);
            stable.Add(horse2);
            stable.Add(horse3);

            
            stable[2].JumpHurdle();
            stable[1].JumpHurdle();
            stable[2].JumpHurdle();

            Console.WriteLine($"{stable[2].Name} has jumped {stable[2].HurdlesJumped} hurdles.");

            // Bird list can hold both Ostrich and Dove.
            List<Bird> aviary = new List<Bird>();
            Ostrich ostr = new Ostrich("Hilda", "Savannah", 200);
            Dove dov = new Dove("Karla", "Dove-place", 72);
            aviary.Add(ostr);
            aviary.Add(dov);

            // Birds can use the method LayEggs to lay eggs.
            foreach (Bird bird in aviary)
            {
                Console.WriteLine($"The bird {bird.Name} has a {bird.Wingspan}cm long wingspan\n" +
                                  $"and prefers the {bird.Habitat} habitat.");
                bird.LayEggs();
                bird.Airstrike();
            }

            // Lets move these animals
            WalkEm(horse1, horse2, horse3, ostr, dov);

            FlyEm(dov);

            // Adding more birds.
            aviary.Add(new Eagle("Henry", "Cliffs", 230));
            aviary.Add(new Eagle("Matilda", "Cliffs", 190));
            aviary.Add(new Goose("Hank", "Pond", 110));
            aviary.Add(new Goose("Betty", "Pond", 115));

            while (true)
            {
                Console.WriteLine("Filter birds by setting a max wingspan:");
                int input = 0;
                try
                {
                    input = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                IEnumerable<Bird> result = 
                    from bird in aviary
                    where bird.Wingspan < input
                    orderby bird.Wingspan ascending
                    select bird;

                foreach (Bird bird in result)
                {
                    Console.WriteLine($"{bird.Name} is a {bird.GetBirdType()}, and has a wingspan of {bird.Wingspan}cm.\n");
                }
            }

        }

        // Method to make animals that can walk, walk.
        static void WalkEm(params IWalker [] walkers)
        {
            foreach (IWalker walker in walkers)
            {
                walker.Walk();
            }
        }

        // Method to make animals that can fly, fly.
        static void FlyEm(params IFlyer [] flyers)
        {
            foreach (IFlyer flyer in flyers)
            {
                flyer.Fly();
            }
        }
    }
}
