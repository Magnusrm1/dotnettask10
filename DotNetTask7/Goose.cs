﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask7
{
    class Goose : Bird, IFlyer
    {
        public Goose(string name, string habitat, int wingspan) : base(name, habitat, wingspan)
        {

        }

        public override void LayEggs()
        {
            Console.WriteLine($"{Name} the goose laid an egg!");
        }

        public override string GetBirdType()
        {
            return "Goose";
        }
    }
}
